

const fs = require('fs');


function createDirectory(dir, callback)
{
    fs.mkdir(dir, err => {
        if(err){
            console.error(err);
        }
        else{
            callback();
        }
    });
}

function writeFile(path, data, callback)
{
    fs.writeFile(path, data, err => {
        if(err){
            console.error(err);
        }
        else{
            callback();
        }
    });
}

function removeFiles(dir)
{
    fs.readdir(dir, (err, data) => {
        if(err){
            console.error(err);
        }
        else{
            data.forEach(file => {
                fs.unlink(dir + file, (err) => {
                    if(err){
                        console.error(err);
                    }
                });
            });
        }
    });
}

function problem1(){
    let data = 'random text';
    const dir = __dirname + '/json/'; 
    createDirectory(dir, () => {                                        
        writeFile(dir + 'file1.json', data, () => {
            writeFile(dir + 'file2.json', data, () => {
                writeFile(dir + 'file3.json', data, () => {
                    removeFiles(dir);
                });
            });


        });
    });   
}


module.exports = problem1;





















//----------------------------------------------------------------------------------------------------------
/*

//problem1 using promises


const fs = require('fs');


function createDirectory(dir, callback)
{
    fs.mkdir(dir, err => {
        if(err){
            console.error(err);
        }
        else{
            callback();
        }
    });
}

function writeFile(path, data, callback)
{
    fs.writeFile(path, data, err => {
        if(err){
            console.error(err);
        }
        else{
            callback();
        }
    });
}

function removeFiles(dir)
{
    fs.readdir(dir, (err, data) => {
        if(err){
            console.error(err);
        }
        else{
            data.forEach(file => {
                fs.unlink(dir + file, (err) => {
                    if(err){
                        console.error(err);
                    }
                });
            });
        }
    });
}



function problem1()
{
    let data = 'random text';
    const dir = __dirname + '/json/'; 

    var promise = new Promise(function(resolve, reject) {     //resolve reject are callback functions here
        
        if(true) 
        {
            resolve();
        } 
        else 
        {
          reject();
        }
      });

      promise.then ( ()=> {
                                          
        writeFile(dir + 'file1.json', data, fs.writeFile(__dirname+ file1.json, data, err => {
            if(err)
            console.error(err);
            }
        
        )) //callback write end
          }) //function write end
       
        
        
      /*  promise.then ( ()=> {
            createDirectory(dir, () => {     //createDirectory ek callback mang raha hai to dena padega                                      
          writeFile(dir + 'file2.json', data)
            })
          });
          
          promise.then ( ()=> {
            createDirectory(dir, () => {     //createDirectory ek callback mang raha hai to dena padega                                      
          writeFile(dir + 'file3.json', data)
            })
          });   
   


    
}//func end 

module.exports=problem1;   */

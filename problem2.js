const fs = require('fs');
const path = require("path");


function readingfile(path, callback) {
  fs.readFile(path, (err, data) => {
    if (err) {
      console.error(err);
    }
    else {
      //console.log(data.toString());
      callback(data);
    }
  });
}




function writingfile(path, data, callback) {
  fs.writeFile(path, data, err => {
    if (err) {
      console.error(err);
    }
    else {
      callback();
    }
  });
}


function appendingFile(path, data, callback) {
  fs.appendFile(path, data, (err) => {
    if (err) {
      console.log(err);
    }
    else
      callback();

  });
}



function f1(data) {
  var read2 = data.toString();
  var read3 = read2.toLowerCase();
  var read4 = read3.split(".");  //read4 has null string in the end


  var read5 = read4.slice(0, read4.length - 1);

  for (let i = 0; i < read5.length; i++) {
    read5[i] = read5[i].trim();
  }



  for (var i = 0; i < read5.length; i++) {
    read5[i] = read5[i].charAt(0).toUpperCase() + read5[i].substring(1);
  }

  var read6 = "";

  for (var i = 0; i < read5.length; i++) {
    read6 = read6 + read5[i] + "."
  }

  return read6;



}


function sort(data) {

  var read6 = data.toString();

  var read7 = read6.split(".");

  for (let i = 0; i < read7.length - 1; i++) {
    read7[i] = read7[i].trim();
  }
  var read8 = read7.slice(0, read7.length - 1);

  read8.sort();


  var read9 = "";

  for (var i = 0; i < read8.length; i++) {
    read9 = read9 + read8[i] + ".";
  }

  return read9;


}


function makeArray(data) {
  var str = data.toString();
  var arr = str.split(".txt");
  var arr1 = arr.slice(0, arr.length - 1);

  for (var i = 0; i < arr1.length; i++) {
    arr1[i] = arr1[i] + ".txt";
  }

  return arr1;
}

function deleteFiles(files, callback) {
  var i = files.length;
  files.forEach(function (filepath) {
    fs.unlink(filepath, function (err) {
      i--;
      if (err) {
        callback(err);
        return;
      } else if (i <= 0) {
        callback(null);
      }
    });
  });
}








function problem2() {

  //Ques-1+2

  readingfile(path.resolve(__dirname, "lipsum.txt"), (data) => {
    console.log("finished reading lipsum.txt");

    var read1 = data.toString().toUpperCase();

    writingfile(path.resolve(__dirname, "newFile1.txt"), read1, () => {

      console.log('newFile-1 is created successfully');


      writingfile(path.resolve(__dirname, "filenames.txt"), "newFile1.txt", () => {

        console.log('filenames.txt is created successfully and has newFile1 added to it');

        //Ques-3
        readingfile(path.resolve(__dirname, "newFile1.txt"), (data) => {

          var received = f1(data);

          writingfile(path.resolve(__dirname, "newFile2.txt"), received, () => {
            console.log('newFile-2 is created successfully');

            appendingFile(path.resolve(__dirname, "filenames.txt"), "newFile2.txt", () => {

              console.log("Appending newFile2 to filenames.txt ");

              //Ques-4(I)

              readingfile(path.resolve(__dirname, "newFile1.txt"), (data) => {

                var received = sort(data);

                writingfile(path.resolve(__dirname, "final.txt"), received, () => {

                  console.log("final.txt created sucessfully with sorted content of newFile-1");
                });//writing

              });//reading   


              //Ques-4(II)

              readingfile(path.resolve(__dirname, "newFile2.txt"), (data) => {

                var received = sort(data);

                appendingFile(path.resolve(__dirname, "final.txt"), received, () => {

                  console.log("Sorted content of newFile-2 added to final.txt");
                  appendingFile(path.resolve(__dirname, "filenames.txt"), "final.txt", () => {


                    console.log("final.txt appended to filenames.txt");

                    //Ques-5


                    readingfile(path.resolve(__dirname, "filenames.txt"), (data) => {

                      var arr = makeArray(data);

                      deleteFiles(arr, () => console.log('all files removed'));

                    }); //reading


                  });//appending
                });//appending
              });//reading   



            }); //appending end
          }); // writing end
        });//reading end




      });// writing end
    }); //writing end
  });//reading end

}

module.exports = problem2;















